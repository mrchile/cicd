FROM php:5.6-apache
ARG user=www
ARG uid=1333
ENV APACHE_RUN_USER ${user}
ENV APACHE_RUN_GROUP ${user}
RUN a2enmod rewrite headers
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN apt-get update && apt-get install -y \
    curl \
    g++ \
    git \
    libbz2-dev \
    libfreetype6-dev \
    libicu-dev \
    libjpeg-dev \
    libmcrypt-dev \
    libpng-dev \
    libreadline-dev \
    libxml2-dev \
    unzip \
    zip  \
    && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-install \
    bcmath \
    bz2 \
    calendar \
    iconv \
    intl \
    mbstring \
    opcache \
    pdo_mysql \
    zip \
    mysql \
    intl \
    xml \
    soap \
    pdo \
    pdo_mysql \
    mysqli
RUN useradd -G www-data -u $uid -d /home/$user $user && \
    mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user
COPY --chown=www ./ /var/www/html
USER $user
RUN composer install --no-scripts
RUN php artisan key:generate
